import React, { ReactNode, useState } from 'react';
import ReactDOM from 'react-dom';
import './index.css';

type SquareValue = 'X' | 'O' | null;

const calculateWinner = (squares: SquareValue[]): SquareValue => { //retorna tipo squarevalue
  const lines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6]
  ];
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i];
    if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
      return squares[a];
    }
  }
  return null;
};

/*Creammos interface pra aquellos que lleven props*/

interface SquareProps {
  onClick(): void;
  value: SquareValue;           //Creamos tipo comun para reutilizar
  //value: 'X' | 'O' | null;    Es un string pero mejor especificar cuál exactamente
}

const Square: React.FC<SquareProps> = props => {
  return (
    <button className='btn' onClick={props.onClick}>
      {props.value}
    </button>
  );
}

interface BoardProps {
  onClick(i: number): void;
  squares: SquareValue[];     //es un array de valor SquareValue
}

const Board: React.FC<BoardProps> = props => {

  const renderSquare = (i: number): ReactNode => {
    return (  //importamos el ReactNode para cargarlo
      <Square
      value={props.squares[i]}
      onClick={() => props.onClick(i)}
    />
    )
  };

  return (
    
    <div className="board">
      <div className="board-row">
        {renderSquare(0)}
        {renderSquare(1)}
        {renderSquare(2)}
      </div>
      <div className="board-row">
        {renderSquare(3)}
        {renderSquare(4)}
        {renderSquare(5)}
      </div>
      <div className="board-row">
        {renderSquare(6)}
        {renderSquare(7)}
        {renderSquare(8)}
      </div>
    </div>)

}


const Game: React.FC = () => {
  const [XisNext, setXisNext] = useState<boolean>(true);
  const [stepNumber, setStepNumber] = useState<number>(0);
  const [history, setHistory] = useState<{squares: SquareValue[]}[]>([
    {
      squares: Array(9).fill(null)
    }
  ]);

  const handleClick = (i: number): void => {
    const timeInHistory = history.slice(0, stepNumber + 1);
    const current = timeInHistory[timeInHistory.length - 1];
    const squares = current.squares.slice();

    if (calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = XisNext ? "X" : "O";
    setHistory(timeInHistory.concat([
      {
        squares: squares
      }
    ]));
    setStepNumber(timeInHistory.length);
    setXisNext(!XisNext);
  };

  const jumpTo = (step: number): void => {
    setStepNumber(step);
    setXisNext(step % 2 === 0);
};

const current = history[stepNumber];
const winner = calculateWinner(current.squares);

const renderMoves = history.map((step, move) => {
  const desc = move ?
    'Go to move ' + move :
    'Go to start';
  return (
    <li key={move}>
      <button className="moves" onClick={() => jumpTo(move)}>{desc}</button>
    </li>
  );
});

let status;
if (winner) {
    status = 'The winner is ' + winner;
} else {
    status = 'Next player is ' + (XisNext ? 'X' : 'O');
}

return (
  <div>
    <div>
      <Board
        squares={current.squares}
        onClick={i => handleClick(i)}
      />
    </div>
    <div className='info'>{status}</div>
      <div className="time-btn">{renderMoves}</div>
    </div>
);
};

ReactDOM.render(
  <React.StrictMode>
    <Game />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
